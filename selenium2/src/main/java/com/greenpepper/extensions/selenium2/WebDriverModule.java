
/**
 * Copyright (c) 2010 Pyxis Technologies inc.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA,
 * or see the FSF site: http://www.fsf.org.
 *
 * @author oaouattara
 * @version $Id: $Id
 */
package com.greenpepper.extensions.selenium2;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.greenpepper.shaded.org.apache.commons.io.IOUtils;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.opera.OperaDriver;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import static org.apache.commons.lang3.StringUtils.*;

public class WebDriverModule
		extends AbstractModule {

	private final String browserType;
	private final String propertiesFile;

	/**
	 * <p>Constructor for WebDriverModule.</p>
	 *
	 * @param browserType the driver type.
	 */
	public WebDriverModule(String browserType, String propertiesFile) {
		this.browserType = browserType;
		this.propertiesFile = propertiesFile;
	}

	/** {@inheritDoc} */
	@Override
	protected void configure() {}

	@Provides
	@Singleton
	private WebDriver createWebDriver() throws IOException {

		WebDriver webDriver;
		if (isBlank(browserType) || equalsIgnoreCase("htmlunit", browserType)) {
			webDriver = getHtmlUnitWebDriver();
		} else if (equalsIgnoreCase("firefox", browserType)) {
			webDriver = getFirefoxWebDriver();
		} else if (equalsIgnoreCase("chrome", browserType)) {
			webDriver = getChromeWebDriver();
		} else if (equalsIgnoreCase("opera", browserType)) {
			webDriver = getOperaWebDriver();
		} else if (equalsIgnoreCase("edge", browserType)) {
			webDriver = getEdgeWebDriver();
		} else if (equalsIgnoreCase("ie", browserType)) {
			webDriver = getInternetExplorerWebDriver();
		} else {
			throw new IllegalArgumentException(String.format("%s is not a supported browser", browserType));
		}
		return webDriver;
	}

	private WebDriver getInternetExplorerWebDriver() throws IOException  {
		Properties properties = new Properties();
		if (isNotBlank(propertiesFile)) {
			ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
			InputStream resourceAsStream = contextClassLoader.getResourceAsStream(propertiesFile);
			if (resourceAsStream != null) {
				properties.load(resourceAsStream);
				MutableCapabilities mutableCapabilities = new MutableCapabilities();
				fillCapabilities(properties, mutableCapabilities);
				return new InternetExplorerDriver(mutableCapabilities);
			} else {
				throw new FileNotFoundException("Unable to find the properties file: " + propertiesFile);
			}
		}
		return new InternetExplorerDriver();
	}

	private WebDriver getEdgeWebDriver() throws IOException  {
		Properties properties = new Properties();
		if (isNotBlank(propertiesFile)) {
			ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
			InputStream resourceAsStream = contextClassLoader.getResourceAsStream(propertiesFile);
			if (resourceAsStream != null) {
				properties.load(resourceAsStream);
				MutableCapabilities mutableCapabilities = new MutableCapabilities();
				fillCapabilities(properties, mutableCapabilities);
				return new EdgeDriver(mutableCapabilities);
			} else {
				throw new FileNotFoundException("Unable to find the properties file: " + propertiesFile);
			}
		}
		return new EdgeDriver();
	}

	private WebDriver getOperaWebDriver()  throws IOException {
		Properties properties = new Properties();
		if (isNotBlank(propertiesFile)) {
			ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
			InputStream resourceAsStream = contextClassLoader.getResourceAsStream(propertiesFile);
			if (resourceAsStream != null) {
				properties.load(resourceAsStream);
				MutableCapabilities mutableCapabilities = new MutableCapabilities();
				fillCapabilities(properties, mutableCapabilities);
				return new OperaDriver(mutableCapabilities);
			} else {
				throw new FileNotFoundException("Unable to find the properties file: " + propertiesFile);
			}
		}
		return new OperaDriver();
	}

	private WebDriver getFirefoxWebDriver() throws IOException  {
		Properties properties = new Properties();
		if (isNotBlank(propertiesFile)) {
			ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
			InputStream resourceAsStream = contextClassLoader.getResourceAsStream(propertiesFile);
			if (resourceAsStream != null) {
				properties.load(resourceAsStream);
				MutableCapabilities mutableCapabilities = new MutableCapabilities();
				fillCapabilities(properties, mutableCapabilities);
				return new FirefoxDriver(mutableCapabilities);
			} else {
				throw new FileNotFoundException("Unable to find the properties file: " + propertiesFile);
			}
		}
		return new FirefoxDriver();
	}

	private WebDriver getHtmlUnitWebDriver() throws IOException {
		Properties properties = new Properties();
		if (isNotBlank(propertiesFile)) {
			ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
			InputStream resourceAsStream = contextClassLoader.getResourceAsStream(propertiesFile);
			if (resourceAsStream != null) {
				properties.load(resourceAsStream);
				MutableCapabilities mutableCapabilities = new MutableCapabilities();
				fillCapabilities(properties, mutableCapabilities);
				return new HtmlUnitDriver(mutableCapabilities);
			} else {
				throw new FileNotFoundException("Unable to find the properties file: " + propertiesFile);
			}
		}
		return new HtmlUnitDriver();
	}

	private void fillCapabilities(Properties properties, MutableCapabilities chromeOptions) {
		if (isNotBlank(propertiesFile)) {
			for (String name : properties.stringPropertyNames()) {
				chromeOptions.setCapability(name, properties.getProperty(name));
			}
		}
	}

	private WebDriver getChromeWebDriver() throws IOException {
		if (isNotBlank(propertiesFile)) {
			ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
			InputStream resourceAsStream = contextClassLoader.getResourceAsStream(propertiesFile);
			if (resourceAsStream != null) {
				ChromeOptions chromeOptions = new ChromeOptions();
				List<String> lines = IOUtils.readLines(resourceAsStream);
				chromeOptions.addArguments(lines);
				return new ChromeDriver(chromeOptions);
			} else {
				throw new FileNotFoundException("Unable to find the properties file: " + propertiesFile);
			}
		}
		return new ChromeDriver();
	}

}
