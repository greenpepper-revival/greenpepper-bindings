{toc}

# Selenium extension for Greenpepper

The project hosted at https://github.com/bonigarcia/webdrivermanager is superb to ease 
the download of the webdriver for selenium. To integrate and use it, the developers 
will have to use the following configuration.

## Dependency

```xml
<dependency>
    <groupId>com.github.strator-dev.greenpepper</groupId>
    <artifactId>greenpepper-extensions-selenium2</artifactId>
    <version>${project.version}</version>
</dependency>
```

## Configuration of the maven plugin 

```xml
<plugin>
  <groupId>com.github.strator-dev.greenpepper</groupId>
  <artifactId>greenpepper-maven-plugin</artifactId>
  <version>${project.version}</version>
  <configuration>
    <systemUnderDevelopment>com.greenpepper.extensions.selenium2.SeleniumSystemUnderDevelopment</systemUnderDevelopment>
    <systemUnderDevelopmentArgs>chrome;true;2.35;conf.properties</systemUnderDevelopmentArgs>
    ...
  </configuration>
</plugin>
```

with the following parameters:

|Position|Description|
|--------|-----------|
| 1      | The browser name. Chosen between: htmlunit, chrome, firefox, edge, opera, ie (defaults to **htmlunit**) |
| 2      | Activate the download of drivers (`true`) or deactivate it (`false` or not set).|
| 3      | Version of the driver to use. You need to check the version compatible with your browser. |
| 4      | A configuration file that should be available in the classpath. This configuration will be inserted in the WebDriver instance. See [Selenium WebDriver Capabilities] |

[Selenium WebDriver Capabilities]: https://github.com/SeleniumHQ/selenium/wiki/DesiredCapabilities

## Interesting links

* WebdriverManager : [https://github.com/bonigarcia/webdrivermanager]()
* Chrome drivers: [https://sites.google.com/a/chromium.org/chromedriver/downloads]()
* Firefox (gecko) drivers: [https://github.com/mozilla/geckodriver/releases]()
* IE drivers : [https://msdn.microsoft.com/fr-fr/library/dn800898(v=vs.85).aspx]()
* EDGE drivers: [https://developer.microsoft.com/en-us/microsoft-edge/tools/webdriver/]()
* Opera drivers: [https://github.com/operasoftware/operachromiumdriver/releases]()
